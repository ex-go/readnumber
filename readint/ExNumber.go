package readint

// Hỗ trợ số tới hàng triệu
func Read(n int) string {
	kq := ""
	var LOP = []string{"", "nghìn", "triệu"}
	var readFirstZero bool

	// Lấy ra từng cum 3 chữ số theo lớp
	for i := 0; n > 0; i++ {
		num3 := n % 1000

		n = n / 1000
		readFirstZero = (n > 0)

		kq = read3(num3, readFirstZero) + LOP[i] + " " + kq

	}
	return kq
}

// Đọc sô có 3 chữ số
func read3(n int, readFirstZero bool) string {
	kq := ""
	const IDX_HANGDV = 0
	const IDX_HANGCHUC = 1
	const IDX_HANGTRAM = 2

	var TEXT1 = [10]string{"không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín"}
	var HANG = [3]string{"", "mươi", "trăm"}

	var soHang [3]int

	// Vòng lặp lấy ra các số hàng đơn vị, hàng chục, hàng trăm.
	for i := 0; (n >= 0 && readFirstZero && i < 3) || (n > 0 && !readFirstZero && i < 3); i++ {
		soHang[i] = n % 10

		// kq = TEXT1[soHang[i]] + " " + HANG[i] + " " + kq

		if (i == 0) && (soHang[i] == 0) { // Cải tiến: Nếu tận cùng là 0 thì không cần đọc
			kq = HANG[i] + " " + kq
		} else if (i == 0) && (soHang[i] == 5) { // Cải tiến: Nếu hàng đơn vị là 5 (và hàng chục không phải là 0) thì đọc là "lăm"
			if n%10 > 0 {
				kq = "lăm" + " " + kq
			} else {
				kq = "năm" + " " + kq
			}
		} else if (i == 1) && (soHang[i] == 1) { // Cải tiến: Nếu hàng chục là số 1 thì đọc là "mười"
			kq = "mười" + " " + kq
		} else if (i == 1) && (soHang[i] == 0) { // Cải tiến: Nếu hàng chục là 0 (và hàng đơn vị > 0) thì đọc là "lẻ"
			if soHang[0] > 0 {
				kq = "lẻ" + " " + kq
			} else {
				// Không làm gì hết
			}

		} else {
			kq = TEXT1[soHang[i]] + " " + HANG[i] + " " + kq
		}

		n = n / 10
	}

	return kq
}
