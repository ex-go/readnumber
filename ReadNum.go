package main

import (
	"fmt"
	"os"
	"readnumber/readint"
	"strconv"
)

func main() {
	fmt.Println("This is tool ReadNumber.")

	if len(os.Args) < 2 {
		fmt.Print("Usage: ReadNum <num>")
		return
	}

	strNum := os.Args[1]
	n, err := strconv.Atoi(strNum)

	if err != nil {
		fmt.Printf("%s is not a numberic.", strNum)
	}
	text := readint.Read(n)

	fmt.Println(text)
}
